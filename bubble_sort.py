class Sort:
    def bubble_sort(self, numbers):
        n = len(numbers)

        for i in range(n):
            for j in range(0, n-i-1):
                if numbers[j] > numbers[j+1]:
                    numbers[j], numbers[j+1] = numbers[j+1], numbers[j]

        return numbers

sort = Sort()

numbers = list(map(int, input("Enter integer numbers separated by space: ").split()))
sorted_numbers = sort.bubble_sort(numbers)
print("Sorted numbers are:", sorted_numbers)