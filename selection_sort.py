class Sort:
    def selection_sort(self, numbers):
        n = len(numbers)

        for i in range(n - 1):
            min_index = i
            for j in range(i + 1, n):
                if numbers[j] < numbers[min_index]:
                    min_index = j

            numbers[i], numbers[min_index] = numbers[min_index], numbers[i]

        return numbers

sort = Sort()

numbers = list(map(int, input("Enter integer numbers separated by space: ").split()))
sorted_numbers = sort.selection_sort(numbers)
print("Sorted numbers are:", sorted_numbers)
