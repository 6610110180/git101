class Sort:
    def insertion_sort(self, numbers):
        for i in range(1, len(numbers)):
            key = numbers[i]
            j = i - 1
            while j >= 0 and key < numbers[j]:
                numbers[j + 1] = numbers[j]
                j -= 1
            numbers[j + 1] = key

        return numbers

sort = Sort()

numbers = list(map(int, input("Enter integer numbers separated by space: ").split()))
sorted_numbers = sort.insertion_sort(numbers)
print("Sorted numbers are:", sorted_numbers)
